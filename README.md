como configurar seu minikube com um bd postgres

kubectl create -f postgres-configmap.yam

kubectl create -f postgres-storage.yaml 

kubectl create -f postgres-deployment.yaml 

kubectl create -f postgres-service.yaml 

para verificar qual porta que é:
kubectl get svc postgres

para conectar
psql -h localhost -U admin --password -p porta db