create table pgprv_usuario(
	id text primary key unique,
	cpf varchar (255),
	nome varchar(255),
	telefone numeric,
	email varchar(255),
	password varchar(255),
	created_at timestamp,
	updated_at timestamp
);

create table pgprv_usr_dados(
	id text not null,
	boleto_valor numeric,
	renda_mensal numeric,
	foto_rg varchar (255),
	foto_endereco varchar (255),	
	aprovado boolean default false,
	created_at timestamp,
	updated_at timestamp,
	constraint usr_dados_id_usr_id_fkey foreign key (id)
		references pgprv_usuario (id) match simple
		on update no action on delete no action
);

create table pgprv_proposta(
	pro_id text primary key unique,
	usr_id text not null,
	instituicao varchar(255),
	boleto text,
	parcelas int4,
	vencido boolean default false,
	curso varchar(255),
	comprovante text,
	created_at timestamp,
	updated_at timestamp,
	constraint pro_id_usr_id_fkey foreign key (usr_id)
		references pgprv_usuario (id) match simple
		on update no action on delete no action
);
	
create table pgprv_boleto(
	bol_id text primary key not null,
	pro_id text not null,
	usr_id text not null,
	numero text,
	valor numeric,
	vencimento date,
	pago boolean default false,
	created_at timestamp,
	updated_at timestamp,
	constraint bol_id_pro_id_fkey foreign key (pro_id)
		references pgprv_proposta (pro_id) match simple
		on update no action on delete no action,
	constraint bol_id_usr_id_fkey foreign key (usr_id)
		references pgprv_usuario (id) match simple
		on update no action on delete no action
);
